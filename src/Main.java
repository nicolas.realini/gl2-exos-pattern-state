import states.TrafficLight;

public class Main {
    public static void main(String[] args) {
        TrafficLight context = new TrafficLight();
        while (context.getCycles() <= 2) {
            context.action();
        }
    }
}
